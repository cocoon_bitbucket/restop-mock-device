import flask
from flask import request
from flask.config import Config
from walrus import Database
from restop_tests.mockserver.mockserver import get_app
#from mockdevice.console import ThreadedMockServer

#= dir_path = os.path.dirname(os.path.realpath(__file__))

platform_name= 'default'

# get config from local config.cfg
config= Config('./')
config.from_pyfile('config.cfg')

http_host= '0.0.0.0'
http_port= config['MOCK_HTTP_PORT']
http_debug= True

redis_host= config['MOCK_REDIS_HOST']
redis_port= config['MOCK_REDIS_PORT']
redis_db= config['MOCK_REDIS_DB']

#console_key= config['MOCK_CONSOLE_KEY']

db = Database(host=redis_host, port=redis_port, db=redis_db)


app = get_app()
app.config.update(config)

app.config['redis_db']= db


@app.route('/mock/console/<queue>', methods=['POST','GET'])
def mock_console_queue(queue,message=None):
    """

    """
    app= flask.current_app
    console= app.config['console']
    if request.method == 'GET':
        # read mock:id:serial
        #message= "GET /custom/serial/stdin"
        message = console.show(queue)
    else:
        message= "/mock/console/<queue>: wrong method"
    return str(message) or "None"




if __name__=="__main__":
    app.run(host=http_host ,port=http_port ,debug=http_debug,threaded=True)