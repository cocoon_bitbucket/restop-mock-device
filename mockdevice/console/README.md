mock_console
============

overview
--------
a process who 

* read orders from a redis stdin queue
* write response to a redis stdout queue
* log actions to a redis logs queue


dependencies
------------

depends on restop framework
