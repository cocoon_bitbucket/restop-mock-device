
import subprocess
import threading

from walrus import List,Hash
from restop_adapters.mock_adapter import MockAdapter

# a redis hash to store state
dashboard_key = "dashboard:mock"


class MockConsole(MockAdapter):
    """


    """

    def _write_stdin(self,cmd):
        """
            write to process stdin , actually buffer

        :param cmd: str with nl
        :return:
        """
        self.log.debug('console [%s] received command: [%s]' % (self.agent_id,cmd))
        # handle a custom command
        lines= self.handle_command(cmd)
        if not lines:
            # launch the command as sent
            lines= subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
        lines= lines or []
        # write to buffer
        queue = List(self.database, self.queue_key('buffer'))
        queue.append(lines)
        return

    def handle_command(self,cmd):
        """

        :param cmd:
        :param kwargs:
        :return:
        """
        return []


    def _read_stdout(self,wait=False):
        """
            read from process stdout  ( actually buffer )
        :param wait:
        :return:
        """
        value= None
        queue = List(self.database, self.queue_key('buffer'))
        if len(queue):
            value = queue.popleft()
        return value


    def dashboard_set(self,key,value):
        """

        :param key:
        :param value:
        :return:
        """
        dashboard= Hash(self.database,dashboard_key)
        dashboard[key]=value
        return True

    def dashboard_get(self,key):
        """

        :param key:
        :return:
        """
        dashboard = Hash(self.database, dashboard_key)
        return dashboard[key]



class ThreadedMockServer(MockConsole,threading.Thread):
    """
        a threading version of adapter

    """
    def __init__(self,agent_id,command_line,**kwargs):
        """

        """
        threading.Thread.__init__(self)
        super(ThreadedMockServer,self).__init__(agent_id,command_line,**kwargs)
        self.Terminated=False
        self.setDaemon(True)



if __name__=="__main__":

    console= MockConsole('mock:id:serial','')

    print "Done"