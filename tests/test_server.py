
from mockdevice.mock_app.mock_starter import app



redis_db=app.config['redis_db']
redis_db.flushdb()

# write to mock console
console= app.config['console']
console.send('ls -l')

# register custom blueprint




if __name__=="__main__":
    app.run( host="0.0.0.0" ,port=app.config['MOCK_HTTP_PORT'] ,debug=False,threaded=True)
