import flask
from flask import Blueprint,request,make_response
from flask import jsonify
from stb.keycodes import key_codes,operations

ALL_METHODS= ['GET', 'POST','PUT','PATCH','DELETE','HEAD','OPTIONS']
custom= Blueprint('custom','custom')

# ardom_filename = "./stb/fixture/ardom_sample.html"
# #ardom_callback_url = "localhost:8080/ardom/tv"
# ardom_callback_url = "localhost:8080/tvbox_agents/feedback-ardom/TV"


@custom.route('/custom', methods=ALL_METHODS)
def custom_1():
    """

    """
    return "custom"


@custom.route('/tvbox_agents/feedback-ardom/<device>' ,methods=['GET', 'POST'])
def post_tvbox_ardom(device='TV'):
    """

    The stb do a GET to this url to send a Dump

    the stb send Dom to the address stored in file:
         /flash/Resources/resources/test_params

        eg: http://192.168.12/feedback/ardom/tv

    we store it in a file ardom-tv.html


    :return:
    """
    filename= "ardom-%s.html" % device
    print "receive an ArDom for device: %s -> %s" % (device,filename)
    data = request.get_data()
    print "ArDom length=%d" % len(data)
    with open( filename,'w') as fh:
        fh.write(data)
    return ""



#
#        stb_send_dump
#        seq1= self.url_for_key("ZOOM",mode=0)
#        seq2= self.url_for_key("RECORD",mode=0)


# /remoteControl/cmd?operation=01&key=018&mode=0
# /remoteControl/cmd?operation=07&id=*****************SUPERCONDRIW0089555_S_2424VIDEO_1&type=0&request=2&code=1111
@custom.route('/remoteControl/cmd', methods=['GET'])
def remote_control_cmd():
    """

    """
    operation = request.args.get('operation')
    _key = request.args.get('key',None)
    _mode = request.args.get('mode',None)
    _id = request.args.get('id', None)
    _type = request.args.get('type', None)
    _request = request.args.get('request', None)
    _code = request.args.get('code', None)

    current_app= flask.current_app
    console= current_app.config['console']

    if operations[operation] == "remote_control":
        # response OK by default
        print "remote control: key=%s , mode=%s" % (_key,_mode)
        console.log.debug("remote control received: remote control: key=%s , mode=%s" % (_key,_mode))

        # detect stb_Send dump
        # seq1 = self.url_for_key("ZOOM", mode=0) ,"ZOOM":372
        # seq2= self.url_for_key("RECORD",mode=0) ,"RECORD":167
        if _key == "372":
            # detect stb_send_dump
            console.log.debug("detect stb_send_dump")
            console.send_ardom()
            # cmd_line = "curl -i -X POST --data-binary @%s %s" % (
            #     ardom_filename, ardom_callback_url)
            # console.send(cmd_line)
        else:
            # detect chang channel key 1 to 9  (513 - 521 )
            channel= int(_key)
            if channel >= 513 and channel <= 521:
                # detect change channel
                channel =channel -512
                # store channel value ( dashboard:mock channel=
                console.dashboard_set('channel', channel)


        response_data= {"result" : {
	        "responseCode" : "0",
	        "message" : "ok",
	        "data" : {}
	        }
        }
        response= jsonify(response_data)
        return response
        #return "remote control: key=%s , mode=%s" %(_key,_mode)



    return "not implemented operation: %s" % operation


# /remoteControl/notifyEvent
@custom.route('/remoteControl/notifyEvent', methods=['GET'])
def remote_control_notify_Event():
    """

    """

    response_data= { "result":
                      {
                        "responseCode": "1",
                        "message": "event notification",
                        "data":
                        { "eventType": "OSD_CONTEXT_CHANGED", "service": "LIVE_FALLBACK" }
                      }
    }
    response= jsonify(response_data)
    return response





