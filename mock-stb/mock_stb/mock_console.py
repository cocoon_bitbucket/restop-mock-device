from mockdevice.console import ThreadedMockServer

import os
ardom_filename_default= "./stb/fixture/ardom_sample.html"
ardom_filename_pattern= "./stb/fixture/ardom_sample_%s.html"


ardom_callback_url = "localhost:8080/tvbox_agents/feedback-ardom/TV"

class MockConsole(ThreadedMockServer):
    """



    """

    def handle_command(self,cmd):
        """

        :param cmd:
        :return:
        """
        return []


    def send_ardom(self):
        """

        :return:
        """
        try:
            # retrieve current channel
            channel= self.dashboard_get('channel')
            filename= ardom_filename_pattern % channel
            os.stat(filename)
        except Exception ,e:
            filename= ardom_filename_default

        self.log.debug("send ardom file: %s" % filename)

        cmd_line = "curl -i -X POST --data-binary @%s %s" % (
            filename, ardom_callback_url)
        self.send(cmd_line)
        return