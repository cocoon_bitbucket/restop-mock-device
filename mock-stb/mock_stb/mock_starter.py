
from mockdevice.http_server.app import app
from mock_blueprint import custom
from mock_console import MockConsole




# register custom blueprint
app.register_blueprint(custom,url_prefix='')

redis_db= app.config['redis_db']
console_key= app.config['MOCK_CONSOLE_KEY']

console= MockConsole(console_key,'',redis_db=redis_db)
console.start()

app.config['console']= console

if __name__=="__main__":
    app.run( host="0.0.0.0" ,port=app.config['MOCK_HTTP_PORT'] ,debug=False,threaded=True)

